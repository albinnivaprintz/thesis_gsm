\section{Weibull simulation study}\label{sec:initsim}

In this section, we will continue the analysis from Example~\ref{ex:gsm}.
We will simulate from a Weibull distribution with a binary and a continuous covariate, and fit both the GSM and AFT models to the data.
We will only consider the special case of the GSM discussed in the example, when it is equivalent to the AFT model.
This will allow us to make a simple performance check of the GSM implemented in \emph{stpm2} compared to the AFT implemented in \emph{survreg}.

\subsection{Data simulation}\label{sec:simwei}

The data were simulated according to the procedure described in Section~\ref{sec:sim}.
Failure times were simulated from the Weibull distribution by the following procedure.
%
\begin{enumerate}
  \item Simulate $n$ uniformly distributed numbers $u_i,\ i=1,\cdots,n$, between $0$ and $1$.
  \item Using the quantile function, $q_W(u)$ of the Weibull distribution with shape $k$ and scale $\lambda e^{- \bs{\beta^T x}}$, the values $q_W(u_i)$ are observations from the Weibull distribution.
\end{enumerate}
%
We used $M = 10$ potential examinations throughout this simulation.
The probability with which each examination is visited was put to $0.9$ with one exception (which will be explained shortly).

We use a total of four different parameter scenarios in this study.
We consider three different distributional forms of the Weibull distribution, with shapes equal to $0.5$, $1$, and $1.5$.
From there, we adjust the scale parameter such that about $90\%$ of the simulated failure times would be expected to occur on the interval $ (0, M] = (0, 10]$.
For some shape $k$, percentage $q$, random variable $T$, and $M$, this is done by solving the equation
%
\begin{equation*}
  F_T \left( M; \lambda, k \right) = q,
\end{equation*}
%
for the scale $\lambda$.
These three scenarios will be referred to as scenarios 1, 2 and 3, respectively.

In the fourth scenario we simulate from the same distribution as in scenario 2, but we decrease the probability by which each measuring time is visited from $0.9$ to $0.6$.
This will result in wider censoring intervals, as well as a larger proportion of right and left censored observations.

With each scenario we also attach two covariates: one binary, and one continuous.
This is done by simulating covariate values for the binary covariate from the bernoulli distribution (with parameter $0.5$) and for the continuous from the standard normal distribution.
These will have the same coefficients for all scenarios.

The three distributions used are visualized in Figure~\ref{fig:distform}, and the parameter values can be found in Table~\ref{tab:simscen}.

\begin{figure}[tb]
  \centering
  \includegraphics[width=0.7\textwidth]{./R/distributionalforms.jpeg}
  \caption{The three different distributional shapes used in the four different parameter scenarios of Section~\ref{sec:initsim}.}
  \label{fig:distform}
\end{figure}

The simulation procedure is summarized as follows:
%
\begin{enumerate}
  \item Simulate $n$ observations from the Weibull distribution with shape $k$ and scale $\lambda e^{-\bs{\beta^T x}}$ (recall the remark in the end of Example~\ref{ex:aft}).

  \item Apply the AFT model using \emph{survreg} from the package \emph{survival} and the GSM model using \emph{stpm2} function from the \emph{rstpm2} package.     

  \item Reparametrize both models using the multivariate delta method (see the appendix Section~\ref{sec:mvd} for a short description of the multivariate delta method), to get estimates and confidence intervals for the shapes, scales, and covariate effects.

  \item Repeat steps 1-3 $N$ times to get $N$ different estimates for each parameter.

  \item Repeat steps 1-4 for each parameter scenario.
\end{enumerate}
%
In this study, we used $n = 500$ and $N = 10^4$.
These values were chosen based on an compromise between available computing power, and obtaining an acceptable level of accuracy.
%
\begin{table}[tb]
  \centering
  \begin{tabular}{r | c c c c}
    Scenario & Shape & Scale & \thead{Binary\\covariate} & \thead{Continuous\\covariate} \\
    \hline
    1 & 0.5 & 1.886 & 0.25 & 0.25 \\
    2 & 1.0 & 4.343 & 0.25 & 0.25 \\
    3 & 1.5 & 5.735 & 0.25 & 0.25 \\
    4 & 1.0 & 4.343 & 0.25 & 0.25 \\
  \end{tabular}
  \caption{Parameter values for the four different simulation scenarios from Section~\ref{sec:initsim}. The first three are different distributional shapes, and in the fourth we simulate from the same distribution as in the second but with a lower probability of each examination being visited, resulting in wider censoring intervals.}
  \label{tab:simscen}
\end{table}
%
\subsection{Analysis and results}

The two functions produce extremely similar results.
The kernel density estimator (for a short introduction, see Section~\ref{sec:kde} of the appendix) of the binary covariate parameter estimator from scenario four can be observed in Figure~\ref{figure:firstsimdens}, and plots for all parameters and scenarios can be found in Figure~\ref{fig:initalldens} in the appendix.
Simply by looking at the distributions of the estimators, one notices how similar they are.

This is further reinforced when inspecting the distribution details of all the parameters in Table~\ref{table:initsim} in the appendix.
The sample mean, sample standard deviations, and 95\% confidence interval coverages are identical for the two models and all parameters.

In Table~\ref{tab:firstresult}, we find the average area between the estimated hazard and true hazard, Akaike information criterion (AIC), covariate parameter estimates, and coverage proportions of the 95\% confidence intervals of the covariate parameters, for both models.
Again, the two models produce strikingly similar results.
One observation that can be made here is that for scenario 2, the area $A(\hat h, h)$ is about 70\% to the area of scenario 4.
This is not very surprising, since even though the two scenarios use the same parameter estimates, scenario four will generally have wider censoring intervals because of the lower examination probability.
This results in less accurate estimators, and thus generally a larger deviance from the true hazard.

The time dependent EMSE plots look identical for all parameter scenarios.
In Figure~\ref{fig:firstoneemse} the time dependent EMSE's of the second parameter scenario is illustrated, while plots for all of the parameter scenarios can be found in Figure~\ref{fig:firstemse} in the appendix.

The confidence interval coverage proportions show no indication of conservative or anti-conservative confidence intervals.
In Table~\ref{tab:firstresult} we observe that the confidence intervals of the two model have the same average coverage proportions.
In fact, we found that both implementations produce equal confidence intervals.

If we want to make inference about the  biases of the covariate parameter estimators, the sixteen different scenario and parameter combinations unfortunately makes us subject to the multiple comparisons problem.
Taking this into consideration, we use the Holm-Bonferroni method \cite{holm1979simple} to adjust the p-values to counteract the issue.
That is, we create a family of $m$ hypotheses $\left\{ 
H_i\right\}$ and their corresponding p-values $p_i$.
Then we sort the hypotheses and p-values in ascending order according to the p-values.
We then compare the $i$th hypothesis to $\frac{\alpha}{m - i}$, $i = 0,\cdots,m-1$, rejecting any null hypotheses with $p_i \leq \frac{\alpha}{m - i}$.
In this case, there are 16 parameter-scenario combinations so we have $m=16$ hypotheses, and we are testing on the level $\alpha=0.05$.

%Using the exact binomial test --- implemented in the \emph{binomial.test} function in R --- we can for each parameter scenario combination test the hypothesis that the 95\% confidence intervals actually contains the true parameter value 95\% of the time.
%If the confidence intervals are 95\%, for any given confidence interval, the event that the true parameter value lies in the confidence interval should be binomially distributed with parameter $\pi = 0.95$.
%So for every parameter and scenario the hypothesis we would like to test is
%
%\begin{align*}
  %H_0:& \pi = 0.95 \\
  %H_a:& \pi \neq 0.95. \\
%\end{align*}
%
%This yields a family of sixteen hypotheses $\left\{ H_i \right\}_i$ and associated p-values $p_i$.
%Using the Holm-Bonferroni method, we did not obtain a significant result.
%That is, we can not reject the null-hypothesis that the 95\% confidence intervals has 95\% coverage.

In order to investigate bias in the parameters we create a family of hypotheses, one for each parameter and scenario.
For the $i$th parameter $\beta_i$ with true parameter value $\beta^{\text{true}}_i$, the hypothesis becomes
%
\begin{align*}
  H_0:& E(\hat\beta_i) = \beta_i \\
  H_a:& E(\hat\beta_i) \neq \beta_i.
\end{align*}
%
Creating t statistics for all scenarios and parameters using the sample means and standard errors from Table~\ref{table:initsim} in the appendix, and testing these using the previously discussed Holm-Bonferroni method, we do not obtain any significant indication of bias in the estimators for a sample size of $500$.

\begin{table}
  \input{./R/result_table_it.tex}
  \caption{A summary of the results from the simulation study of \emph{stpm2} and \emph{survreg} on Weib$(k, \lambda)$ distributed censored data. Visualized in the table is the area difference, degrees of freedom, AIC, average parameter estimates and confidence intervals, and 95\% confidence interval coverage proportions for each covariate.}
  \label{tab:firstresult}
\end{table}

\begin{figure}[tb]
  \centering
  \includegraphics[width=0.5\textwidth,trim={14cm 0 28cm 0},clip]{./R/Plots/first_all_density_4_shift.png}
  \caption{The kernel density estimates of the binary covariate parameter estimator for both the GSM and AFT models from parameter scenario 2 of Section~\ref{sec:initsim}. The dotted line represents the true parameter value.}
  \label{figure:firstsimdens}
\end{figure}

\begin{figure}[tb]
  \centering
  \includegraphics[width=0.6\textwidth]{./R/EMSE_2.png}
  \caption{The time dependent empirical square mean error (EMSE) plots for parameter scenario 2. The two implementations produce identical EMSE for all examined values of $t$.}
  \label{fig:firstoneemse}
\end{figure}



\section{Likelihood theory}

\subsection{Non-informative censoring}\label{sec:noninfo}

In the case of exact data, the contribution of an observation $t$ to the likelihood $L(\bs\theta)$ for parameters $\bs\theta$ is simply $f_T(t|\bs\theta)$.
It would seem reasonable to assume that the contribution of observing an observation censored on $[L,R]$ is simply $P(T \in [L,R])$, and under certain circumstances this is indeed the case.
It turns out \cite{oller2004interval} that this holds under the assumption of \emph{non-informative censoring}.
In this paper we will not go into very much mathematical detail of this assumption, but merely try to give the reader some intuition.

We will use the informal definition of non-informativity presented on page 42 in \cite{kleinbaum2010survival}.
The assumption states that the distribution of failure times yields no information about the distribution of censoring intervals, and vice versa.

Now, one might wonder if there are any cases of \emph{informative censoring} in practice.
The answer is yes, and this will hopefully be clarified in the following two examples.

Let us revisit the fictional infant study previously discussed in Section~\ref{sec:cens}.
Consider a situation where the event of a child learning to walk made the parents of said child lose interest in the study, and drop out before the study was over.
Then the distribution of the failure times heavily influences the distribution of the censoring intervals, since no examinations would be visited after the event occurred.
This is a case of informative censoring, since the failure times influence the distribution of the censoring intervals.

Another possible scenario would be if the examination process of the children had a significant impact on their walking performance.
Then the children attending more examinations would learn to walk faster than the other children.
Or in other words, the distribution of censoring intervals yields information about the failure times, and thus the censoring is informative.

A mathematical definition of the non-informativity assumption can be found in \cite{oller2004interval}.

There are two other assumptions usually made about the censoring, \emph{random censoring} and \emph{independent censoring} \cite{kleinbaum2010survival}.
These treat the randomness of the censoring process, and basically assume that the hazards of censored units are the same as those of the non censored units over the whole data set, as well as conditioned on covariates.
These assumptions will not be discussed in more detail, since these assumptions are automatically fulfilled if the failure times and censoring times are simulated independently.

\subsection{Likelihood theory for censored data}\label{sec:likecens}

When we have exact observations, the contribution of an observation to the likelihood function is $f_T(t|\bs\theta)$.
Under the assumption of non-informative censoring, the contribution of an observation censored on the interval $[L, R]$ to the likelihood is $P_{T|\bs\theta}(L \leq T < R)$.
Expressing this probability in terms of the survival function, we get that
%
\begin{equation}
  P_{T|\bs\theta}(L \leq T < R) = S(L|\bs\theta) - S(R|\bs\theta).
  \label{eq:llcontrib}
\end{equation}
%
In the special cases right and left censoring of interval censoring, \eqref{eq:llcontrib} simplifies to $S(L|\bs\theta)$ and $1-S(R|\bs\theta) = F(R|\bs\theta)$ respectively, recalling that $S(0) = 1$ and $\lim_{t \to \infty} S(t) = 0$. 
The censoring type of an observation and the likelihood contribution is summarized in Table~\ref{table:lhcontrib}.

\begin{table}[ht]
  \center
  \begin{tabular}{l l}
    Censoring type & Likelihood contribution \\ \hline
    Exact, $L = R = t$ & $f(t|\bs\theta)$ \\
    Interval censored, $[L, R]$ & $S(L|\bs\theta) - S(R|\bs\theta)$ \\
  Left censored, $[0, R]$ & $1 - S(R|\bs\theta) = F(R|\bs\theta)$ \\
  Right censored, $[L, \infty)$ & $S(L|\bs\theta)$
  \end{tabular}
  \caption{The different types of censored observations, and their likelihood contributions.}
  \label{table:lhcontrib}
\end{table}

Let $\bs\theta$ be a parameter vector, $E$ be the set of exact observations, $L$ the set of left censored observations, $R$ the set of right censored observations, and $I$ be the set of censored observations.
The likelihood and log likelihood function becomes
%
\begin{equation}
  L(\bs\theta| E, L, R, I) = \prod\limits_{t \in E} f(t|\bs\theta) \prod\limits_{r \in L} (1 - S(r|\bs\theta)) \prod\limits_{l \in R} S(l|\bs\theta)\prod\limits_{(l,r) \in I} (S(l|\bs\theta) - S(r|\bs\theta)),\label{eq:likeli} \\
\end{equation}
\begin{align}
  l(\bs\theta| E, L, R, I) = \sum\limits_{t \in E} \log f(t|\bs\theta) &+ \sum\limits_{r \in L} \log\left\{ 1 - S(r|\bs\theta) \right\} \nonumber\\
  &+ \sum\limits_{l \in R} \log\left\{ S(l|\bs\theta)\right\} \nonumber\\
  &+ \sum\limits_{(l, r) \in I} \log\left\{ S(L|\bs\theta) - S(R|\bs\theta) \right\}.
  \label{eq:logli}
\end{align}
%
The parameter vector $\bs{\hat\theta}$ which maximizes the likelihood function (or equivalently the log likelihood function) is called the \emph{maximum likelihood (ML) estimator} of $\bs\theta$ \cite{held2013applied}.
The ML-estimator can be calculated with the use of the derivative and second derivative of the (log) likelihood function.
As per usual, the \emph{score vector} is obtained by taking the gradient of the log likelihood 
%
\begin{equation*}
  \nabla l(\bs\theta|E, L, R, I) = \left( \frac{\partial l(\bs\theta|E, L, R, I)}{\partial \theta_i} \right)_{i}.
\end{equation*}
%
The \emph{Fisher information matrix} is obtained by taking the negative hessian of the log likelihood
%
\begin{equation*}
  I(\bs\theta|E, L, R, I) = - \left( \frac{\partial^2l(\bs\theta|E, L, R, I)}{\partial \theta_i\partial\theta_j} \right)_{i,j}.
\end{equation*}

\begin{addmargin}[2em]{2em}
  \begin{exmp}
    For this example, we will consider exponentially distributed failure times $T_i \sim \text{Exp}(\lambda)$ with mean $\lambda^{-1}$.
    Given $n_E$ exact observations, and $n_I$ interval censored observations we get explicit expressions of the likelihood and log likelihood using Equation \eqref{eq:likeli} and \eqref{eq:logli}.
%
    \begin{align*}
      L(\lambda| E, I) &= \lambda^{n_E} \prod\limits_{t \in \bs E} e^{-\lambda t} \prod\limits_{(L, R) \in \bs I} \left( e^{-\lambda L} - e^{-\lambda R} \right),\\
      l(\lambda| E, I) &= n_E \log \lambda -  n_E \lambda \bar{t} + \sum\limits_{\left( L,R \right) \in I} \log\left( e^{-\lambda L} - e^{-\lambda R} \right),
    \end{align*}
%
    where $\bar{t} = \frac{1}{n_E}\sum_{t \in E} t$.
    % A reader with a trained eye might get a little worried at the look of the log likelihood.
    As we shall now see, the last sum does not simplify.
    The score function is calculated by differentiating the log-likelihood with respect to $\lambda$,
%
    \begin{equation*}
      \nabla l(\lambda| E, I) = \frac{n_E}{\lambda} - n_E \bar{t} - \sum\limits_{\left( L,R \right) \in I} \frac{Le^{-\lambda L} - R e^{- \lambda R}}{e^{-\lambda L} - e^{-\lambda R}}.
    \end{equation*}
%
    Without the censored observations the solution to the score equation can be expressed analytically, but the censoring clearly complicates the expression.
    The Fisher information becomes
%
    \begin{align*}
      I(\lambda| E, I) &= -\frac{d}{dt}\left( \nabla l(\lambda| E, I) \right)  \nonumber\\
      &= \frac{2n_E}{\lambda} - \sum\limits_{(L,R) \in I} \frac{\left( L^2e^{-\lambda L} - R^2e^{-\lambda R} \right)\left( e^{-\lambda L} - e^{-\lambda R} \right) }{\left( e^{-\lambda L} - e^{-\lambda R} \right)^2} \nonumber \\
      &\qquad\quad\,\,-\sum\limits_{(L,R) \in I} \frac{\left( Le^{-\lambda L} - Re^{-\lambda R} \right)^2}{\left( e^{-\lambda L} - e^{-\lambda R} \right)^2}.
    \end{align*}
%
    The expressions for the score and information are not presented here in expectation that the reader will have any use of these expressions, but merely to illustrate the fact that they will require a numerical solution.
    This is also the reason for going with the exponential function for this example, the expression for the Fisher information matrix in the case of Weibull failure times is more complex.

    In practice the score and Fisher information can also be calculated using finite differences in the log likelihood.
    For example, in one variable we have
%
    \begin{align*}
      \nabla l(\theta) &\approx \frac{l(\theta + h) + l(\theta - h)}{2h}, \\
      I(\theta) &\approx \frac{l(\theta + h) - 2l(\theta) + l(\theta - h)}{h^2},
    \end{align*}
% 
    for sufficiently small $h$.

  \end{exmp}
\end{addmargin}




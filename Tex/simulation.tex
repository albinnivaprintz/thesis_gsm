\section{Simulation methods}\label{sec:sim}

We will simulate interval censored data according to prescription 1, p.675 in~\cite{lawless2006models}, also discussed in~\cite{gomez2009tutorial}.
The idea is to generate examination times in some manner, and then independently of that simulate failure times from the desired distribution.
Examination times are the moments when we observe the unit.
If a failure occurs in between two examinations, it is censored on that interval.
Different interval lengths are obtained by each unit missing out on each examination with some predestined probability $p$.
In the next paragraph the simulation method is described in more detail.

Let $N$ be the total number of observations we want to simulate, indexed by $i$.
%
\begin{enumerate}
  \item Simulate $N$ failure times $t_1, t_2,\dots,t_N$ from a distribution with some known parameter $\theta$.
    This step is described specifically for the Weibull distribution and mixed Weibull distribution in Sections~\ref{sec:simwei} and~\ref{sec:simmixwei} respectively.

  \item Generate $M$ potential examination times $C_j,\ j = 1,2,\dots,M$.
    In Section~\ref{sec:initsim} we simply use $C_j = j$, while in Section~\ref{sec:mix} we consider more frequent examinations as well.
    These potential examination times are the same for all units in the sample.

  \item For every unit, each potential examination is visited with some probability $p$.
    The largest measure time smaller than $t_i$ becomes $L_i$, the lower bound of the censoring interval.
    The smallest measure time larger than $t_i$ becomes $R_i$, the upper bound of the censoring interval.

\end{enumerate}
%
This method can produce intervals of different lengths by adjusting the visiting probability $p$, and produces left, right, and interval censored data.
If there is no visited measure time before $t_i$, then the observation is left censored.
Similarly, if there is no visited measure time after $t_i$, the observation becomes right censored.
If none of the measure times are visited, the observation is discarded.

Recall the earlier discussion on non-informativity in Section~\ref{sec:noninfo}.
The simulation method presented in this section satisfies the non-informativity assumption, since the measure times and failure times are simulated independently.
An example of a simulation method subject to informative censoring could look something like this:

Simulate failure times $T_i$ and generate potential measure times $C_{ij},\ j=1,\dots,M$ exactly like in the previous proposed method.
Now, for the $i$th unit, the $j$th measure time $C_{ij}$ is visited with the probability $p$ if $C_{ij} < T_i$, and zero otherwise.
With this simulation method the censoring intervals are clearly dependent on the failure times, and therefore informative.


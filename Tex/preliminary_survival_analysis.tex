\section{Concepts for survival analysis}

We will introduce some concepts that are common in the analysis of time-to-event data.
This mainly consists of defining what we actually mean by censored data, and some useful functions.
The definitions presented in this section are adapted from Cox and Oakes~\cite{cox1984analysis}.

\subsection{The survival, hazard and cumulative hazard functions}\label{sec:defi}

Throughout this paper, we are going to refer to the survival, hazard, and cumulative hazard functions.
There are several different ways of representing these in terms of each other, some of which are presented in Section~\ref{subsec:connectsurhaz}.
All of these functions refer to a non-negative, continuous random variable $T$.
We will regularly omit subscripting functions of random variables when the underlying random variable is obvious from context, i.e.\ write $f(t)$ instead of $f_T(t)$.

\subsubsection{Survival function}

We begin by defining the survival function.

\begin{addmargin}[2em]{2em}
  \begin{defi}[Survival function, p. 13 in~\cite{cox1984analysis}]\label{def:surv}
    The survival function --- denoted $S_{T}(t)$ --- of a random variable $T$, is defined as
%  
    \begin{equation}\label{eq:surv}
      S_{T}(t) = P_{T}(T > t) = \int\limits_t^{\infty} f_T(x) dx.
    \end{equation}
%
\end{defi}
\end{addmargin}

That is, $S(t)$ is the probability that the event happens after time $t$ --- or, conversely, that the event does not happen by time $t$.
The connection between the survival function and the cumulative distribution function $F(t)$ is simply that
%
\begin{equation*}
  S(t) = 1 - F(t).
\end{equation*}
%
As a consequence of this relation, the survival function is monotonically decreasing, and satisfies $S(0) = 1$ and $\lim\limits_{t \to \infty} S(t) = 0$.
This follows directly from the corresponding properties of the cumulative distribution function. Note that improper distributions may also be possible, such as due to statistical cure, where $\lim\limits_{t \to \infty} S(t) > 0$.

\begin{addmargin}[2em]{2em}
\begin{exmp}% [Weibull distribution]

  The Weibull distribution is a common choice for parametric survival analysis models.
  We will use it as an example throughout this paper to illustrate certain concepts.
  A Weibull distributed random variable $T$ with scale parameter $\lambda$ and shape parameter $k$ has density function
%
  \begin{equation*}
    f_T(t) = \frac{kt^{k-1}}{\lambda^k} e^{-(t/\lambda)^k},\quad t, \lambda, k\geq0.
  \end{equation*}
%
  Using Equation~\ref{eq:surv}, we can calculate the survival function of $T$:
%
  \begin{equation*}
    S_T(t) = \int\limits_t^\infty \frac{kx^{k-1}}{\lambda^k} e^{-(x/\lambda)^k}\ dx = e^{-(t/\lambda)^k}.
  \end{equation*}
%
\end{exmp}
\end{addmargin}

\subsubsection{Hazard function}

Another function that appears in time-to-event analysis is the hazard function.
The interpretation of the hazard function is less intuitive than the survival function.

\begin{addmargin}[2em]{2em}
\begin{defi}[Hazard function, p. 14 in~\cite{cox1984analysis}]\label{eq:hazarddef}

  The hazard function of a random variable $T$ is defined as the instantaneous rate of event occurrence:
%
  \begin{equation}
    h_T(t) = \lim\limits_{\Delta t \to 0} \frac{P_T(T < t + \Delta t\ |\ T \geq t)}{\Delta t}.
  \end{equation}
%
\end{defi}
\end{addmargin}

It is the conditional limiting probability of the event occurring in the interval $[t, t+\Delta t)$, divided by the length of the interval, as the length of the interval goes to zero.
The hazard function is closely related to the density function $f(t)$, as will be presented shortly.

Sometimes one also has use of the cumulative hazard function $H(t)$, defined by
%  
\begin{equation}\label{eq:cumhaz}
  H(t) = \int\limits_0^{t} h(t') dt',
\end{equation}
%
which, as we shall see, has a close connection to the survival function.

\subsubsection{Connecting $S(t)$ and $h(t)$}\label{subsec:connectsurhaz}

Expanding the conditional probability in the numerator of Definition~\ref{eq:hazarddef} using the definition of conditional probability, the following expression for the hazard function is obtained:
%
\begin{equation}\label{eq:hazfs}
  h(t) = \lim\limits_{\Delta t \to 0} \frac{P(t \leq T < t + \Delta t)}{\Delta t \cdot S(t)} = \frac{f(t)}{S(t)}.
\end{equation}
%
\noindent which, if one notices that the derivative of $S(t)$ with regards to $t$ is $-f(t)$, then the hazard function can be re-expressed as:
%
\begin{equation*}
  h(t) = - \frac{d}{dt} \log S(t).
\end{equation*}
%
Using this formula to rewrite the survival function in terms of the hazard function, we obtain
%
\begin{equation}\label{eq:StoH}
  S(t) = \exp \left\{ - \int\limits_0^{t} h(t') dt' \right\} = \exp \left\{ - H(t) \right\}.
\end{equation}
%
\begin{addmargin}[2em]{2em}
\begin{exmp}
  
  Continuing with the Weibull distribution, we can use Equation~\eqref{eq:hazfs} to calculate the hazard of $T \sim \text{Weibull}(k, \lambda)$:
%
  \begin{equation}\label{eq:weib:haz}
    h(t) = \frac{f(t)}{S(t)} = \frac{kt^{k-1}}{\lambda^k}.
  \end{equation}
%
  The cumulative hazard function can either be calculated by integrating the hazard, like in Equation~\eqref{eq:cumhaz}, or using Equation~\eqref{eq:StoH}.
  Either way, we will arrive at the cumulative hazard function:
%
  \begin{equation*}
    H(t) = - \log(S(t)) = \int\limits_0^t h(t') dt' = \left(\frac{t}{\lambda}\right)^k.
  \end{equation*}
%
\end{exmp}
\end{addmargin}


\subsection{Censored data}\label{sec:cens}

In survival analysis one attempts to model the time to some event from a longitudinal study, which gives rise to a special kind of imprecision in the data.
Consider a hypothetical study on new born children investigating the time until the child learns to walk.
Usually parents notice when their toddler starts wandering away, but for the sake of this example let us assume the children in the study have especially inattentive parents.
A natural way to collect these data would be to select some infants, observing their walking skills once a month until they can successfully take a few steps.
However, the time when a child actually learned to walk could be at any point in time in between the last test before the first successful test, and the first successful test.
This is the idea of \textit{interval censored data}. \\

Let us put this in more mathematical terms.
The participants in the study will throughout the paper be referred to as \emph{units}.
When talking about a \emph{failure time}, we mean the occurrence of the event of interest.
The times the units are observed will be referred to as \emph{examinations}.
In the previous example, the time the $i$th child learned to walk would be referred to as the failure time of the $i$th unit.
Denote the failure time of the $i$th unit by $T_i$, the last examination before $T_i$ by $L_i$, and the first examination after $T_i$ by $R_i$.
The concept is visualized in Figure~\ref{figure:intervaldata}. \\

Other types of censoring are left- and right-censored data.
A right censored observation is only known to have happened after some point in time, and a left censored observation is only known to have happened before some point in time.
Right and left censored data occur as special cases of interval censored data.
With right censored data we get the interval $[L_i, \infty)$, and with left censored data we get the interval $(0, R_i]$.

\begin{figure}[ht]
  \centering
  \includegraphics[width=0.7\textwidth, clip, trim={0 140mm 0 100mm}]{./Illustrations/interval-data-visual.pdf} 
  \caption{A visualization of censored data, displaying the end points of the censored interval $L_i$ and $R_i$, as well as the actual failure time $T_i$.}\label{figure:intervaldata}
\end{figure}


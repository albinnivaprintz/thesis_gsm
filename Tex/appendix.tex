\section{Appendix}

\subsection{Theory}

\subsubsection{Censoring granularity}

Here follows a short discussion about the effect of censoring granularity on the \emph{stpm2} knot placement.

As discussed in Section~\ref{sec:gsm}, the knots of \emph{stpm2} are placed on the quantiles of the observed left censoring limit in the case of interval and right censored data, and on the right censoring limit in the case of left censored data.
For one simulated data set, the knot placements for one, up to and including five knots, for different censoring interval placement resolutions can be found in Table~\ref{tab:knotplac}.
In the study we used the step length $0.1$ and examination probability $0.5$.
This yields (approximately) an expected interval length (given that the observation is interval censored) of $0.3$.
Employing a step length of for example, one, with the same probability of examination will yield significantly longer censoring intervals.
Similarly using a shorter step length between each possible examination will yield much shorter censoring intervals.

\begin{table}[hb]
  \centering
  \begin{tabular}{c|c|lllll}
    Censoring parameters & knots & \multicolumn{5}{c}{Knot placement}\\
    \hline
    $M = 10$   & 1 & 4     &       &       &      &\\
    $p = 0.9$  & 2 & 2     & 6     &       &      &\\
    $s = 1$    & 3 & 2     & 4     & 7     &      &\\
               & 4 & 2     & 3     & 5     & 8    & \\
               & 5 & 1     & 2     & 4     & 6    & 9 \\
    \hline
    $M = 10^2$ & 1 & 4.2   &       &       &      &\\
    $p = 0.5$  & 2 & 2.5   & 6.8   &       &      &\\
    $s = 0.1$  & 3 & 1.9   & 4.2   & 8.7   &      &\\
               & 4 & 1.5   & 3.2   & 5.6   & 9.7  & \\
               & 5 & 1.3   & 2.5   & 4.2   & 6.8  & 9.8 \\
    \hline
    $M = 10^3$ & 1 & 4.325 &       &       &      &\\
    $p = 0.06$ & 2 & 2.58  & 6.80  &       &      &\\
    $s = 0.01$ & 3 & 1.96  & 4.325 & 8.72  &      &\\
               & 4 & 1.64  & 3.19  & 5.66  & 9.94 & \\
               & 5 & 1.43  & 2.58  & 4.325 & 6.8  & 9.98 \\
  \end{tabular}
  \caption{Knot placement for different censoring parameters and different number of knots. Notice that the expected interval censoring length is different for the three scenarios, but yet the knot locations are fairly similar.}
  \label{tab:knotplac}
\end{table}

\subsubsection{Kaplan-Meier estimator} \label{def:km}

In this paper our focus is directed at parametric survival models.
A common non-parametric estimator of the survival function is the Kaplan-Meier (KM) estimator\cite{kaplan1958nonparametric}.
We use the KM estimator in Section~\ref{sec:tandmob} to visualize the Tandmobiel data.
The KM estimator will not be described in detail, since it is not central to the results of this thesis.
In the case of non-censored failure times the KM-estimator is calculated as follows.

The failure times are divided into $n$ intervals $t_i = [a_i, b_i),\ i = 1,\dots,n$, such that at least one event occurs at $a_i$, the beginning of each interval.
The KM-estimate of the survival curve then takes the form
%
\begin{equation*}
  \hat{S}(t) = \prod\limits_{i;\ \min t_{i} < t} \left( 1 - \frac{e_i}{r_i} \right),
\end{equation*}
%
where $e_i$ are the number of events in time interval $t_i$, and $r_i$ are the number of units at risk in the beginning of time interval $t_i$.
In the case of interval censored observations, Turnbull presented an iterative algorithm which converges to the ML-estimate \cite{turnbull1974nonparametric}\cite{survival-package}.
The KM and Turnbull estimators are implemented in \verb|survfit.formula::survival| in R.

\subsubsection{Multivariate delta method}
\label{sec:mvd}

The multivariate delta method\cite{held2013applied} is a method of calculating the distribution of transformations of the mean of i.i.d, $p$-dimensional random variables.
Let $\bar{\bs{Y}}_n = \frac{1}{n} \sum\limits_{i = 1}^n \bs{X}_i$ be the mean of $n$ i.i.d random variables $\bs{X}_i$ with expectation $\bs\mu < \infty$ and covariance matrix $\bs\Sigma$.
Furthermore, let $f$ be a function from $\mathbb{R}^p$ to $\mathbb{R}^q$ with $q \leq p$ which is continuously differentiable in a neighborhood of $\bs\mu$, with $p \times q$ Jacobian matrix $\bs{J}$.
Then
%
\begin{equation*}
  \sqrt{n} \left( f(\bs{Y}_n) - f(\bs\mu) \right) \stackrel{\text{apprx}}{\sim} \text{N}\left( \bs{0}, \bs{J^t \Sigma J} \right),\quad n \to \infty.
\end{equation*}
%
In this thesis we use the multivariate delta method to transform parameter estimates produced by different models to the same parameterization.

\subsubsection{Kernel density estimator}
\label{sec:kde}

The kernel density estimator is a non-parametric estimator of the density function some random variable.
We introduce the concept of a kernel and take a look at the definition.
A kernel function $K$ is simply any non-negative function which satisfies
%
\begin{equation*}
  \int\limits_{-\infty}^{\infty}K(x)dx = 1.
\end{equation*}
%
Given a kernel function $K$, $n$ observations $x_i,\ i = 1, \dots, n$ of some random variable $X$, and a bandwidth parameter $h > 0$, the kernel density estimator of the density $f$ of $X$ is
%
\begin{equation*}
  \hat f_h (x) = \frac{1}{n h}\sum\limits_{i = 1}^n K\left( \frac{x - x_i}{h} \right).
\end{equation*}
%
There are many possible choices of kernel.
In this paper, we chose the gaussian kernel $\phi$.

\subsection{Plots and tables}

\begin{figure}[tb]
  \centering
  \begin{subfigure}[b]{\textwidth}
    \centerline{\includegraphics[scale=0.3]{./R/Plots/first_all_density_1.png}}
    \caption{Scenario 1: Shape 0.5 and probability 0.9}
  \end{subfigure}

  \begin{subfigure}[b]{\textwidth}
    \centerline{\includegraphics[scale=0.3]{./R/Plots/first_all_density_2.png}}
    \caption{Scenario 2: Shape 1 and probability 0.9}
  \end{subfigure}

  \begin{subfigure}[b]{\textwidth}
    \centerline{\includegraphics[scale=0.3]{./R/Plots/first_all_density_3.png}}
    \caption{Scenario 3: Shape 1.5 and probability 0.9}
  \end{subfigure}

  \begin{subfigure}[b]{\textwidth}
    \centerline{\includegraphics[scale=0.3]{./R/Plots/first_all_density_4.png}}
    \caption{Scenario 4: Shape 1 and probability 0.6}
  \end{subfigure}

  \caption{The kernel density estimations for scenarios and both models from section~\ref{sec:initsim}. Notice that they look very similar between the models. This is a good thing since it indicates that the models are implemented in a satisfactory manner. Theoretically, they should be the same.}
  \label{fig:initalldens}
\end{figure}

\begin{table}[ht]
  \centering
  \input{./R/init_sim_table_in_thesis.tex}
  \caption{Details on the distribution of the estimators of \emph{stpm2} and \emph{survreg} from the analysis of Section~\ref{sec:initsim}. Again, we notice that the two implementations produce identical results.}
  \label{table:initsim}
\end{table}

\begin{figure}[ht]
  \centering
  \begin{subfigure}[b]{0.48\textwidth}
    \includegraphics[width=\textwidth]{./R/EMSE_1.png}
    \caption{Scenario 1.}
  \end{subfigure}
  ~
  \begin{subfigure}[b]{0.48\textwidth}
    \includegraphics[width=\textwidth]{./R/EMSE_2.png}
    \caption{Scenario 2.}
  \end{subfigure}

  \begin{subfigure}[b]{0.48\textwidth}
    \includegraphics[width=\textwidth]{./R/EMSE_3.png}
    \caption{Scenario 3.}
  \end{subfigure}
  ~
  \begin{subfigure}[b]{0.48\textwidth}
    \includegraphics[width=\textwidth]{./R/EMSE_4.png}
    \caption{Scenario 4.}
  \end{subfigure}
  \caption{The time dependent empirical square mean error (EMSE) plots for the four different parameter scenarios of Section~\ref{sec:initsim}.}
  \label{fig:firstemse}
\end{figure}

\begin{figure}[ht]
  \centering
  \includegraphics[width=\textwidth]{./R/Plots/mixed_cov_estimates01.pdf}
  \caption{The kernel density estimates of the covariate parameter distributions of the two models from Section~\ref{sec:mix}.}
  \label{fig:mixcovdist}
\end{figure}


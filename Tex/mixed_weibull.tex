\section{Mixed Weibull simulation study}\label{sec:mix}

In the previous section we examined the performance of the two implementations \emph{stpm2} and \emph{survreg} of the AFT model, in the case of an underlying Weibull distribution.
This is of course an interesting and important study to make, but it does not showcase the possible need for the GSM\@.
If all failure times followed a standard distribution, then the AFT and PH models would always suffice.
In many practical settings in biostatistics, this is unlikely to be the case.
In this section, we are going to assess the performance of the Weibull PH implemented in \emph{survreg}, compared to the GSM in \emph{stpm2}.

We would expect that the GSM would outperform the PH, since it defaults back to the Weibull PH when using zero knots.
This is an ideal case though, and will depend on the implementations of the two models.

\subsection{Mixed Weibull distribution}

The underlying distribution of the failure times in this section will be a mixture of Weibull distributions.
This distribution has been used in previous simulation studies \cite{rutherford2015use} to evaluate the performance of spline based models.

The idea of the mixture distribution is simple.
Let $m_i \in [0,1],\ \sum_{i = 1}^n m_i = 1$ be $n$ \emph{mixing parameters}, $k_1,\ldots, k_n$ shape parameters, and $\lambda_1,\ldots,\lambda_n$ scale parameters.
Also let $f_i(t)$ be the density of a Weibull distribution with parameters $k_i$ and $\lambda_i$.
We say that a random variable $T$ is mixture of Weibull distributions with parameters $m_i,\ k_i,\ \lambda_i,\ i=1,\ldots, n$ if it has the density function
%
\begin{equation}
  f_{T}(t) = \sum\limits_{i = 1}^n m_i f_i(t),
  \label{eq:mixweidens}
\end{equation}
%
\noindent for $t > 0$, and $0$ for $t\leq 0$.
In this section, we will refer to the $f_{i}(t)$s as \emph{component distributions}.
% So really it is exactly what the name suggests, a mixture between $n$ component Weibull distributions.

Expressions for the survival function and hazard function of $T$ can easily be calculated from the density:
%
\begin{align}
  S_T(t) &= \sum\limits_{i = 1}^n m_i e^{- \left(\frac{t}{\lambda_i}\right)^{k_i}} \\
  h_T(t) &= \frac{\sum\limits_{i = 1}^n m_i \frac{k_i}{\lambda_i}\left( \frac{t}{\lambda_i} \right)^{k_i-1} e^{- \left( \frac{t}{\lambda_i} \right)^{k_i}}}{\sum\limits_{i = 1}^n m_i e^{- \left(\frac{t}{\lambda_i}\right)^{k_i}}}
  \label{eq:mixweisurvhaz}
\end{align}
%
The nice properties of the Weibull distribution transfer naturally to the mixed Weibull distribution.
For example, since all the $m_i$s sum to one, and the density function of any Weibull distribution integrate to one from $0$ to $\infty$, we have that
%
\begin{equation}
  \int\limits_0^{\infty} f_T(s) ds = 1
\end{equation}
%
for a mixed Weibull distributed random variable $T$.

\subsection{Simulation of mixed Weibull distributed data}\label{sec:simmixwei}

Using the definition of the PH model (Definition~\ref{defi:ph}), and the equations presented in Section~\ref{subsec:connectsurhaz} we can rewrite the PH assumption to
%
\begin{equation*}
  S\left(t | \bs x\right) = S\left( t | \bs 0\right)^{e^{\bs{\beta^T x}}}.
\end{equation*}
%
From here, we can use the inverse of the previous stated function to generate the failure times.
Below follows a more detailed description of the method.
%
\begin{enumerate}
  \item Simulate $n$ uniformly distributed numbers $u_j,\ i = 1,\ldots,n$ between $0$ and $1$.
  \item Adjust the uniformly simulated values according to their corresponding covariate values $\tilde{u}_j = u_j^{e^{-\bs{\beta^T x}}}$.
  \item Let $W$ be of the baseline mixed Weibull distribution, and $q_W$ its quantile function.
  The values $q_W(\tilde{u}_j)$ are now observations from $W|X = x$.
\end{enumerate}
%
The quantile function of the mixed Weibull distribution can not be easily calculated.
Instead we use the root finding function \emph{uniroot} in R to find the value of $S(t|0)$ which equals $\tilde{u}_j$.

In this study we will consider failure times $T$ with the mixture of two Weibulls as baseline distribution, more specifically $T_0 \sim \text{MixWei}(m = 0.7, k_1 = 1.9, k_2 = 2.5, \lambda_1 = 11.29, \lambda_2 = 1.62)$.
This is the same distribution used in Scenario 4 in \cite{rutherford2015use}.
This distribution is unimodal with a right shoulder, and the density and hazard are visualized in Figure~\ref{fig:dhmix}.

In order to capture the more complicated features of the mixed Weibull distribution, we will use more detailed potential examination times $C_i = i \times 10^{-1},\ i = 1,\ldots,160$.
Define the step length of a censoring technique to be the shortest distance between any pair of possible examination times.
In this case, the step length is $10^{-1}$.
To compensate for the increase in potential examination times, we will use a lower probability of each potential examination actually being visited to $p = 0.5$.
%
\begin{figure}[htb]
  \centering
  \begin{subfigure}[b]{0.4\textwidth}
    \includegraphics[scale=0.12]{./R/mixdensity.jpeg}
    \caption{Density}
  \end{subfigure}
  \quad
  \begin{subfigure}[b]{0.4\textwidth}
    \includegraphics[scale=0.12]{./R/mixhazard.jpeg}
    \caption{Hazard}
  \end{subfigure}
  \caption{The density and hazard of the mixed Weibull distribution considered in Section~\ref{sec:mix}, $T \sim \text{MixWei}(m = 0.7, k_1 = 1.9, k_2 = 2.5, \lambda_1 = 11.29, \lambda_2 = 1.62)$.}
  \label{fig:dhmix}
\end{figure}
%
As in the previous section the units will be under the effect of two covariates, one binary and one continuous.
We use different values of the covariate parameters though.
In this section we use $0.5$ for both parameters.

500 simulated values from the baseline distribution are visualized in Figure~\ref{fig:mixft}.

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.8\textwidth]{{./R/Plots/mixed_ftdist0.1}.pdf}
  \caption{Visualizing the failure times from the mixed Weibull simulation study. Each line segment represents the interval an observation in censored on. The observations are sorted on their left interval limits. Notice the group of right censored observations in the top right corner.}
  \label{fig:mixft}
\end{figure}

\subsection{Method}

In order to fit a GSM allowing for more than zero knots we need a way to determine the amount of knots to use.
We will use AIC as a knot count selection tool.
More precisely, we will fit the GSM with $0,\ldots,8$ knots and then choose the model with the highest AIC\@.
This is not a very fast procedure to repeat a lot of times.
A faster but less fair approach would be to test the optimal knot count on a small number of simulated data sets, and then simply test for the two most common knot count, or even just using the most common knot count (see the \verb+logspline+ package on CRAN for an approach that smooths on the log hazard scale with knot selection).

Due to numerical instability, we did not calculate the area between the estimated and true hazard between the minimum and maximum of the simulated failure times.
The two models yield numerical overflow for estimated hazards that are outside the possible censoring interval.
In order to compensate for this, we instead measure the area between the first simulated failure time and the last possible examination time.
The area will in this section then measure the performance of the two models up to the last possible examination time.

\subsection{Performance results}

Unlike in the last section, the two models do not display the same behavior when applied to the mixed Weibull data.
The PH model implemented in \emph{survreg} misses the complex features of the mixed Weibull distribution.
The GSM implemented in \emph{stpm2} is often able to capture these features, placing knots as discussed in Section~\ref{sec:gsm} and fitting natural splines to the observed values of $-\log\left( -\log\left( S(t) \right) \right)$.

The simulation results are summarised in Table~\ref{tab:mixresult}.
We note that the GSM produces a lower mean area and AIC than the Weibull PH, indicating a better fit.
Also, the mean covariate parameter estimates of the GSM are slightly closer to the true values compared with those from the PH model.
The covariate parameter kernel density estimates of the two models can be observed in Figure~\ref{fig:mixcovdist} in the appendix.
The two models seem to produce similar, but slightly different covariate estimates.
In Table~\ref{tab:mixresult} we see that both models produce slightly biased covariate estimates for this sample size, except for \emph{survreg} and the binary covariate.
%This is further supported by the mean estimates in Table~\ref{tab:mixresult}, where the estimates from \emph{stpm2} lie lightly closer to the true values than \emph{survreg}.
%However, we can test if the expected differance in distance from the true parameter values is significantly different from zero using a standard t-test.
%Denote the expectation of the $i$;th covariate parameter estimate from the $j$;th model by $\mu_{i,j}$.
%Formally, the null hypothesis and alternative hypothesis we are testing for the $i$;th covariate parameter are
%%
%\begin{align}
%  H_{0,i}:& \quad|\mu_{\text{i, \emph{stpm2}}} - \beta_{\text{i}}| - |\mu_{\text{i, \emph{survreg}}} - \beta_{\text{i}}| = 0, \nonumber\\
%  H_{a,i}:& \quad|\mu_{\text{i, \emph{stpm2}}} - \beta_{\text{i}}| - |\mu_{\text{i, \emph{survreg}}} - \beta_{\text{i}}| \neq 0.
%  \label{eq:mixhyp}
%\end{align}
%%
%Denoting the mean of the observed values for the $i$;th parameter $\bar{x}_i$ and similarly its standard error by se$(x_i)$, we create the t statistic
%%
%\begin{equation*}
%  \frac{\bar{x}_i - 0}{\text{se}(x_i) / \sqrt{n}} \sim t(n - 1),
%\end{equation*}
%%
%we obtain that the expected difference is negative ($p_{\text{bin}} < 10^{-4}$, $p_{\text{cont}} < 10^{-4}$).
%That is, for this particular distribution the GSM and \emph{stpm2} outperforms the AFT in \emph{survreg} in terms of covariate parameter estimation.
%
\begin{table}[tb]
  \input{./R/Plots/mix_result_table_it.tex}
  \caption{Summarising the results of the simulation study. The mean area between the estimated hazard and the true hazard, degrees of freedom, AIC, and means and standard errors of the covariate parameters are displayed. Besides the covariate headers, their true values are displayed.}
  \label{tab:mixresult}
\end{table}

In Figure~\ref{fig:mixsurvhazest} representatives of the estimated survival and hazard functions are drawn together with the true curves.
The representatives were chosen at random, but were applied to the same simulated data set.
Observe how in Figure~\ref{fig:mixhazest} the Weibull PH model simply cuts through the peak of the true hazard function, while the GSM does a better job of capturing this feature.

\begin{figure}[tb]
  \centering
  \begin{subfigure}[b]{0.48\textwidth}
    \includegraphics[width=\textwidth]{./R/Plots/mixed_survival01.pdf}
    \caption{The survival function.}
  \end{subfigure}
  ~
  \begin{subfigure}[b]{0.48\textwidth}
    \includegraphics[width=\textwidth]{./R/Plots/mixed_hazard01.pdf}
    \caption{The hazard function.}
    \label{fig:mixhazest}
  \end{subfigure}
  \caption{Illustrated here are examples of the estimated and the true hazard and survival functions for the two models. The examples were chosen because they have area differences close to the observed mean area difference and were fitted on the same data set.}
  \label{fig:mixsurvhazest}
\end{figure}

In Figure~\ref{fig:mixemse}, the time dependent EMSE is plotted for both models.
The figure also indicates that the GSM in \emph{stpm2} does a better job of capturing the more complex features of the distribution.

\begin{figure}[tb]
  \centering
  \includegraphics[width=0.8\textwidth]{./R/Plots/mixedEMSE01.png}
  \caption{The time dependent empirical mean square error (EMSE) of the two models implemented on the simulated mixed Weibull data. The GSM (\emph{stpm2}) seems to perform much better than the Weibull PH (\emph{survreg}).}
  \label{fig:mixemse}
\end{figure}

\subsection{Varying the censoring parameters}\label{sec:censres}

In the mixed Weibull simulation study the possible examinations were more frequent than in the ordinary Weibull case, in order to capture the more advanced features of the more complex distributional shape.
In actual studies, control of examination frequency is often limited due to financial or practical aspects.
In this section we examine the behavior of \emph{stpm2} when we change the censoring resolution.
We consider three cases:

\begin{enumerate}
  \item High resolution: step length: $10^{-2}$, probability of examination visit: $0.5$.
  \item Medium resolution: step length: $10^{-1}$, probability of examination visit: $0.5$.
  \item Low resolution: step length: $1$, probability of examination visit: $0.5$.
\end{enumerate}

In each of the 500 simulation iterations we simulate 500 observations.
The failure times for one of the simulation iterations of the three censoring cases are visualized in Figure~\ref{fig:mix3ft}.

Intuitively, the more exact the data is, the more complicated behavior can be captured by \emph{stpm2}.
This is visualized in Figure~\ref{fig:mix3emse}, where we see how the EMSE of \emph{stpm2} decrease drastically with the higher censoring resolution while the performance of \emph{survreg} barely changes.
The application of \emph{stpm2} on the step length 1 data stands out as much worse than the other two with regards to EMSE\@.
This suggests a limit at which any efforts to further shorten the censoring intervals leaves one subject to diminishing returns.
The length of the censoring intervals determines the magnitude of failure time distribution characteristics one is able to detect.

\begin{figure}[tb]
  \centering
  \begin{subfigure}[b]{0.4\textwidth}
    \includegraphics[width=\textwidth]{./R/Plots/mixed_ftdist1.pdf}
    \caption{Step length $1$.}
  \end{subfigure}
  ~
  \begin{subfigure}[b]{0.4\textwidth}
    \includegraphics[width=\textwidth]{{./R/Plots/mixed_ftdist0.1}.pdf}
    \caption{Step length $10^{-1}$.}
  \end{subfigure}
  ~
  \begin{subfigure}[b]{0.4\textwidth}
    \includegraphics[width=\textwidth]{{./R/Plots/mixed_ftdist0.01}.pdf}
    \caption{Step length $10^{-2}$.}
  \end{subfigure}
  \caption{Failure times for three different censoring resolution scenarios of Section~\ref{sec:censres}. One thing to note is that the proportion of left censored observations decreases with increasing censoring resolution.}
  \label{fig:mix3ft}
\end{figure}

\begin{figure}[tb]
  \centering
  \begin{subfigure}[b]{0.48\textwidth}
    \includegraphics[width=\textwidth]{./R/Plots/mixedEMSE1.png}
    \caption{Step length $1$.}
    \label{fig:mix3a}
  \end{subfigure}
  ~
  \begin{subfigure}[b]{0.48\textwidth}
    \includegraphics[width=\textwidth]{./R/Plots/mixedEMSE01.png}
    \caption{Step length $10^{-1}$.}
    \label{fig:mix3b}
  \end{subfigure}
  ~
  \begin{subfigure}[b]{0.48\textwidth}
    \includegraphics[width=\textwidth]{./R/Plots/mixedEMSE001.png}
    \caption{Step length $10^{-2}$.}
    \label{fig:mix3c}
  \end{subfigure}
  \caption{EMSE for the three different censoring resolution scenarios of Section~\ref{sec:censres}. From Subfigure~\ref{fig:mix3a} to Subfigure~\ref{fig:mix3b} the EMSE decreases significantly. This is not the case when increasing the resolution further.}
  \label{fig:mix3emse}
\end{figure}

The covariate estimates continue to exhibit slight bias, except for \emph{survreg} with step length 0.1 and the binary covariate.

\begin{table}[tb]
  \input{./R/Plots/mix_result_table3.tex}
  \caption{AIC, area difference, and covariate estimates and confidence intervals for the three different censoring resolution cases of Section~\ref{sec:censres}.}
    \label{tab:mix3tab}
\end{table}



  \section{Survival analysis models}

  In this section we present three parametric survival analysis models.
  In Section~\ref{def:km} of the appendix we describe the non-parametric Kaplan-Meier estimator of the survival function.

  %We are used to perform regression on a variety of different things.
  %In linear regression we model directly on some value from the real world, house prices for example.
  %In log linear models, we model on counts in cells of contingency tables.
  %In logistic regression models we model on probabilities.
  %In the parametric survival analysis models we are going to discuss, we are modeling on the functions presented in Section~\ref{sec:defi}.
  %An example is the proportional hazards model, introduced in the next subsection, where we perform regression on the survival function.

  \subsection{Proportional hazards model}

  Before we can look at a more general model, we will introduce the proportional hazards (PH) model.
  In this model, one assumes that the hazard function $h_x(t)$ of $t$ under the effect of a covariate $x$ is proportional to the baseline hazard $h_0(t)$.
  Let us clearly define what we mean by this.

  \begin{addmargin}[2em]{2em}
  \begin{defi}[Proportional hazards model, p.70 in~\cite{cox1984analysis}]\label{defi:ph}
    Let $\bs x$ be a vector of covariates, $h_{T}(t;\bs x)$ be the hazard function of a random variable $T$ given $\bs x$, and $h_{T}(t;\bs 0)$ be the baseline hazard of $t$.
    By employing a \emph{proportional hazards model}, we assume the effect of covariates satisfies the relation
%
    \begin{equation*}
      h_{T}(t;\bs x) = h_{T}(t;\bs 0)\psi(\bs x),\quad \psi(\bs 0) = 1.
    \end{equation*}
%
  \end{defi}
  \end{addmargin}
%
  The function $\psi$ is a link function between the covariates and their effect.
  In this thesis, we will always put $\psi(\bs x) = e^{\bs{\beta}^T \bs{x}}$ for some parameter vector $\bs \beta$ and covariate vector $\bs x$.
  We see that for any covariate vector $\bs x$, $h(t;\bs x) \propto h(x;\bs 0)$, which is the source of the name proportional hazards model.

  This model yields a simple interpretation of covariate effects.
  Given a binary covariate and a parameter $\beta = \log(2)$, under the assumption of the PH we can state that the hazard of those under covariate effect is always twice that of those not under covariate effect. 

  \begin{addmargin}[2em]{2em}
    \begin{exmp}\label{ex:ph}
    Recall that the hazard function of a Weibull-distribution with scale parameter $\lambda$ and shape parameter $k$ is
%
    \begin{equation*}
      h_0(t) = \frac{kt^{k-1}}{\lambda^k}.
    \end{equation*}
%
    Under the PH model we find, using Definition~\ref{defi:ph}, the hazard of $t$ under covariate effect from $\bs x$ to be
%
    \begin{equation}\label{eq:weibhaz}
      h_x(t) = \frac{kt^{k-1}}{\lambda^k}e^{\bs{\beta^T x}}.
    \end{equation}
%
    In Figure~\ref{figure:prophaz:haz} the two hazard functions $h_0(t)$ and $h_1(t)$ are illustrated for the special case when $\bs x$ is a binary covariate, $\beta = 1$, $\lambda = 1$, and $k = 1.4$.

    The expressions for the cumulative hazard, log cumulative hazard, and survival function can be derived from Equation~\eqref{eq:weibhaz} and the relations in Section~\ref{subsec:connectsurhaz}.
%
    \begin{align}
      H(t|\bs x,\bs \beta) &= \int\limits_0^t h(t'|\bs x, \bs\beta) dt' = \frac{t^k}{\lambda^k}e^{\bs{\beta^T x}} \nonumber\\
      \log(H(t| \bs x, \bs \theta)) &= k\log(t) - k\log(\lambda) + \bs{\beta^T x}\label{eq:logHlin} \\
      S(t| \bs x, \bs \theta) &= \exp\left\{ -H(t|\bs x,\bs \beta) \right\} = \exp\left\{ - \frac{t^k}{\lambda^k}\exp\left\{\bs{\beta}^T \bs{x}\right\} \right\}.\nonumber
    \end{align}
%
    Taking one extra look at Equation~\eqref{eq:logHlin}, we notice that the effect of covariates is linear on the log cumulative hazard scale.

  \end{exmp}
  \end{addmargin}


  \begin{figure}[htb]
    \center
    \includegraphics[width=0.8\textwidth]{./R/prophaz-haz.png} 
    \caption{Illustrated are the hazard functions $h_0(t)$ and $h_1(t)$ from Example~\ref{ex:ph}, as well as the ratio $\frac{h_1(t)}{h_0(t)}$ between them. Notice how because of the proportional hazards effect the ratio between the two hazard functions is constant.}\label{figure:prophaz:haz}
      \end{figure}

  \subsection{Accelerated time model}

  Another common parametric survival model is the accelerated failure time model (AFT).
  The idea behind the AFT is that the time to failure of a unit under effect of covariates $\bs x$ is accelerated by some constant amount $\psi(\bs x)$.
  Let us define what we mean by this.

  \begin{samepage}
  \begin{addmargin}[2em]{2em}\label{defi:aft}
    \begin{defi}[Accelerated failure time model, p. 64 in~\cite{cox1984analysis}]
      Denote the survival function of a random variable $T$ affected by covariates in $\bs x$ by $S_T(t;\bs x)$.
      By employing the \emph{accelerated time model} we assume that the effect of covariates can be described by the relation

      \begin{equation*}
	S_T(t;\bs x) = S_T\left( \psi(\bs x)\, t; \bs 0 \right).
      \end{equation*}

    \end{defi}
  \end{addmargin}
\end{samepage}
%
  Just like with the PH model, we will always let $\psi(\bs x) = e^{\bs{\beta^T x}}$ in this paper.

  Expressions for the density, hazard, and cumulative hazard of a unit under covariate influence can be derived from their definitions:
%
  \begin{align}
    f_T(t; \bs x) &=  \psi(\bs x) f_T\left( \psi(\bs x) t;\bs 0 \right) \label{eq:aftdens}\\
  h_T\left( t;\bs x \right) &= \psi(\bs x) h_T\left( \psi(\bs x) t; \bs 0 \right)\label{eq:afthaz} \\
  H_T\left( t;\bs x \right) &= H_T(\psi(\bs x)t;\bs 0)\label{eq:aftcumhaz}
\end{align}
%
One way to think of the AFT assumption is that the underlying random variable (time to failure) of a unit under covariate effect $T_{\bs x}$ is identically distributed to $T_{\bs 0}/\psi(\bs x)$.
This also yields the interpretation of covariate effects.
An example is if we have a binary covariate, with parameter value $\beta = \log 2$.
Then, a unit under the influence of the covariate cuts the expected time to failure in half compared to if it was not under covariate effect.

\begin{addmargin}[2em]{2em}
  \begin{exmp}\label{ex:aft}
    Let us assume that the AFT model holds for some independent and identically distributed failure times $T_i$ under the effect of covariates $\bs x$, and assume a baseline Weibull distribution.
    That is,
%
    \begin{equation*}
      S_{\bs 0}(t) = \exp\left\{ -\left( t/\lambda \right)^k \right\}.
    \end{equation*}
%
    Under the AFT, the survival, density, hazard, and cumulative hazard functions conditioned on a covariate vector $\bs x$ is found to be
%
    \begin{align*}
      S_{\bs x}(t) &= \exp\left\{ -\left( \frac{t}{\lambda e^{-\bs{\beta^T x}}} \right)^k  \right\} \\
      f_{\bs x}(t) &= \frac{k}{\lambda e^{-\bs{\beta^t x}}} \left( \frac{t}{\lambda e^{-\bs{\beta^T x}}} \right)^{k-1} \exp\left\{ -\left( \frac{t}{\lambda e^{-\bs{\beta^T x}}} \right)^k  \right\}\\
      h_{\bs x}(t) &= \frac{k}{\lambda e^{-\bs{\beta^t x}}}\left( \frac{t}{\lambda e^{-\bs{\beta^T x}}} \right)^{k-1} \\
      H_{\bs x}(t) &= \left( \frac{t}{\lambda e^{-\bs{\beta^T x}}} \right)^k,
    \end{align*}
%
    using Definition~\ref{defi:aft} and Equations~\eqref{eq:aftdens},~\eqref{eq:afthaz}, and~\eqref{eq:aftcumhaz}.
    Writing this is terms of the log cumulative hazard and reparametrizing $\bs{\tilde{\beta}} = k\bs\beta$ we obtain
%    
    \begin{equation*}
      \log\left( H_{\bs{x}}(t) \right) = k \log t - k\log \lambda + \bs{\tilde\beta^T x}
    \end{equation*}
%
    Notice that this is exactly the same as the PH model when assuming a baseline Weibull distribution derived in Example~\ref{ex:ph}.
    In general, this is not the case.
    Under the AFT model, and Weibull baseline distributed failure times $T_0 \sim \text{Weib}\left( k, \lambda \right)$, covariate affected failure times are distributed according to $T_{\bs x} \sim \text{Weib}\left( k, \lambda e^{-\bs{\beta^T x}} \right)$.

  \end{exmp}
\end{addmargin}
\subsection{Generalized survival model}\label{sec:gsm}

As the name suggests, the generalized survival model (GSM) is a generalization of parametric survival models.
It is more general in the sense that it allows for a much wider variety of distributions of the failure times than, for example, the proportional hazard model can offer.

\begin{addmargin}[2em]{2em}
  \begin{defi}[Generalized survival model, section 2.1 in~\cite{liu2016parametric}]\label{def:gsm}

  Let $g$ be a link function, $t$ be a realization of a failure time (positive random variable) $T$, $\bs x$ a covariate vector, and $\bs \theta$ a parameter vector.
  The generalized survival model takes the form
%  
  \begin{equation}\label{eq:gsm}
    g(S(t|\bs x;\bs\theta)) = X(t, \bs x)\bs\theta = \eta(t, \bs x; \bs \theta) \\
  \end{equation}
  where $X$ is the model matrix, and $\eta$ is a linear predictor. \\

\end{defi}
\end{addmargin}
%
We could express the linear predictor as $\eta(t, \bs x; \bs \theta) = g(S_0(t)) + \bs{\beta^T x}$, which expresses the covariate effects as being \emph{additive}.
Note that the model matrix $X$ is time dependent, where the linear predictor is a function of time. Hopefully the example in the end of this section will help to illustrate this.

What assumptions are we putting on the link function?
Well, as the name suggests, the purpose of a link function is to act as a \emph{link} between the linear predictor (whose image can be all of $\mathbb{R}$) and in this case, the survival function (whose image is the interval $(0,1)$).
Then it seems like a necessity that such a function is invertible, since we want to be able to retrieve the survival function given the linear predictor, just as well as we can obtain the linear predictor given the survival function.
We will use the same assumptions on the link function made by Younes and Lachin~\cite{younes1997link} when they first presented the link-based model that the GSM stems from.
Thus, we assume that the link-function $g: (0,1) \to \mathbb{R}$ is a bijective, strictly monotone function and known.
This yields that $g$ is invertible.
We also make another assumption about $g$ that is not made in~\cite{younes1997link}, and that is that the inverse link function is differentiable with respect to $t$.
The reason for this will become apparent when we write the hazard function under the GSM\@.

The linearity of $\eta$ refers to the fact that it is linear with respect to the parameter vector $\bs\theta$.
From the model matrix representation of the GSM, the linearity is captured by the matrix multiplication of $X$ with $\bs\theta$, while the way in which the model matrix depends on time may be complex.
This is hopefully clarified in the example at the end of this section.

One assumption that we make about $\eta$ is that it is twice differentiable with respect to time.
The need for the once differentiable assumption will (just as with the link function) become apparent when we try to write the hazard function under the GSM\@.
In biological settings, it is common to assume that the underlying hazard ``changes smoothly with time'' \cite{younes1997link}.
Taking smooth to mean differentiable, we will see that this means exactly that the linear predictor should be twice differentiable in the next paragraph.

Let $G = g^{-1}$ be the inverse link function.
Using the Equation~\eqref{eq:gsm}, and the connections between the survival, hazard, and cumulative hazard found in Section~\ref{subsec:connectsurhaz}, we find explicit expressions for them:
%
\begin{align}
  S(t|\bs x; \bs \theta) &= G(\eta(t,\bs x;\bs\theta)) \nonumber\\
  H(t|\bs x; \bs \theta) &= - \log\left( G(\eta(t,\bs x;\bs\theta)) \right) \nonumber\\
  h(t|\bs x; \bs \theta) &= - \frac{G'(\eta(t,\bs x;\bs\theta))}{G(\eta(t,\bs x;\bs\theta))}\, \eta'_t(t,\bs x;\bs\theta).\label{eq:gsmhaz}
\end{align}
%
Here we see the reason for differentiability of the inverse link function and the twice differentiability of the linear predictor.
If they were not, the hazard function would not be defined and differentiable under the GSM.

In \emph{stpm2}, the default is that the time effects in the linear predictor are modelled as natural splines for log time \cite{younes1997link}.
That is, we assume that the true baseline hazard can be expressed as
%
\begin{equation*}
g(S_0(t))  = s^d_K (\log t)
\end{equation*}
%
where $s^d_K (\log t)$ is a spline of degree $d$ over knots in the set $K$.
It is not necessary to model on the log time scale, but it has been suggested a good choice for the most common link functions\cite{liu2016parametric}.
A natural spline is a picewise polynomial function, with a zero second derivative in the left- and rightmost knots.
The values of $t$ where the polynomial pieces meet are called knots, and the spline is required to be continuous and twice differentiable at these knots (recall the remark made earlier in this section about the underlying linear predictor changing smoothly with respect to time).
Given a degree $d$, and $|K|$ knots in the set $K$, there exists a \emph{basis spline} having the property that all splines $s_K^d(t)$ can be written as a linear combination
%
\begin{equation*}
  s_K^d(\log t) = \sum\limits_{i = 1}^{|K| - 1} \theta_i B_i^d(\log t)
  \label{eq:bsp}
\end{equation*}
%
where $B_i^d(\log t)$ is a basis function with natural spline properties. In R, the natural splines are calculated by the \verb+splines::ns()+ function using a matrix projection of B-splines.
An algorithm for constructing the B-splines is presented in \cite{younes1997link}.
Having found a basis, we fit the spline by adjusting the parameters $\theta_i$.
In order to enforce positivity of the hazard, we use a quadratic penalty to ensure that $\eta'_t(t,\bs x;\bs\theta)>0$ \cite{liu2016parametric}. One of the advantages of the GSMs is that the calculation of survival does not require integration, while the calculation of the hazard requires  differentiation of the linear predictor with respect to time.

% Once the baseline hazard has been estimated, we can use the results of Section~\ref{sec:defi} to express the baseline survival function by
% %
% \begin{equation*}
%   S_0(t) = \exp\left\{ - \int\limits_0^t h_0(s) ds \right\}.
% \end{equation*}
% %
% An expression for the survival under covariate effect can now be found using Definition~\ref{sec:defi}.

Wold \cite{wold1974spline} proposed that knot number and placement should not be considered ordinary parameters of the curve fitting process, but instead thought of as deciding the underlying functional form.
Instead, Wold suggested that knots should be placed in areas where data density is high, or places where it attains a maximum or minimum.
In \emph{stpm2} the knots are placed on the observed failure time quantiles, a strategy which has been suggested when nothing is known about the underlying distribution the data \cite{younes1997link}.
In the case of censored observations the knots are placed on the quantiles of the left censoring interval boundary if the unit is interval or right censored, and on the right censoring interval boundary if the unit is left censored.
The number of knots to use can be determined using forward selection on AIC \cite{younes1997link}.

\begin{addmargin}[2em]{2em}
  \begin{exmp}\label{ex:gsm}
    Consider this special case of the GSM:\\
    Let $\bs x$ be a covariate vector of length $n$, and
%
    \begin{align*}
      \bs\theta &= \left( k, \alpha, \beta_1,\dots,\beta_n \right)^T \\
      g(S(t)) &= \log(-\log(S(t))) \\
      \eta(t, \bs x; \bs \theta) &= k\log t + \alpha + \bs{\beta^T x}.
    \end{align*}
%
    Notice that the linear predictor is differentiable with respect to time, $\eta'_t(t,\bs x;\bs\theta) = \frac{k}{t}$.
    Also, the link function has a differentiable inverse, $G(\eta(t,\bs x;\bs\theta)) = \exp\left\{ - \exp\left\{ \eta(t,\bs x;\bs\theta) \right\} \right\}$.

    Then, the model matrix and parameter vector for $n$ covariates and $m$ units (denoting the $j$th covariate of the $i$th unit by $x_{ij}$) is simply
%
    \begin{equation*}
      X(t, \bs x) = \begin{pmatrix}
	\log t & 1 & x_{11} & \dots & x_{1n} \\
	\log t & 1 & x_{21} & \dots & x_{2n} \\
	\log t & 1 & x_{31} & \dots & x_{2n} \\
	\vdots & \vdots & \vdots & \ddots & \vdots \\
	\log t & 1 & x_{m1} & \dots & x_{mn}
      \end{pmatrix}
    \end{equation*}
    %
    and
    %
    \begin{equation*}
      \begin{pmatrix}
	k \\
	\alpha\\
	\beta_1\\
	\vdots\\
	\beta_n
      \end{pmatrix}.
    \end{equation*}
    %
    Now, using equation~\eqref{eq:gsmhaz} we can find an expression for the hazard function:
%
    \begin{equation*}
      h(t|\bs x,\bs\theta) = kt^{k-1}e^\alpha e^{\bs{\beta^T x}}.
    \end{equation*}
%
    Reparametrizing $\alpha = -k\log\lambda$ we get the hazard function of the Weibull distribution under PH (or equivalently AFT) effects, presented in Examples~\ref{ex:aft} and~\ref{ex:ph}.
    That is, this special case of the GSM is equivalent to the AFT and PH models.
    This fact will be used to compare the GSM implemented in the R package \emph{rstpm2} to the AFT in the standard R package \emph{survival} in Section~\ref{sec:initsim}.
    
  \end{exmp}
\end{addmargin}


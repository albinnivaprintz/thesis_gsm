library(reshape2)
library(ggplot2)
source('sim.R')

x <- seq(from = 0.01, to = 15, by = 0.01)
f <- dweibull(x, 0.5, reasonablescale(10, 0.9, 0.5))
s <- dweibull(x, 1, reasonablescale(10, 0.9, 1))
t <- dweibull(x, 1.5, reasonablescale(10, 0.9, 1.5))

d <- data.frame(x = x,
                f = f,
                s = s,
                t = t)
colnames(d) <- c('t', '1', '2', '3')
d <- melt(d, id.vars = 't',
          value.name = 'Density',
          variable.name = 'Scenario')

ggplot(d, aes(x = t)) +
  geom_line(aes(y = Density, col = Scenario, linetype = Scenario), size = 1.2) +
  ylim(0, 0.3) +
  xlim(0, 15) +
  scale_linetype_manual(values=c('solid', 'dashed', "dotdash"))
ggsave('distributionalforms.jpeg', width = 181.2, height = 105.8, device = 'jpeg', units = 'mm')
